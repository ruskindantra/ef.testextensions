using System;
using System.Collections.Generic;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using Moq;
using Xunit;

namespace RuskinDantra.Ef.TestExtensions.UnitTests
{
    public class ef_test_extensions
    {
        [Fact]
        public void should_be_able_to_return_db_set()
        {
            var contextSimulatorMock = new Mock<IContextSimulator>();
            Action subject = () => contextSimulatorMock.Setup(c => c.SampleEntities).Returns(new List<sample_entity>().ToDbSetMock().Object);
            subject.Should().NotThrow();
        }
    }

    public interface IContextSimulator
    {
        DbSet<sample_entity> SampleEntities { get; set; }
    }

    public class context_simulator : IContextSimulator
    {
        public virtual DbSet<sample_entity> SampleEntities { get; set; }
    }

    public class sample_entity
    {
        
    }
}